# WIP notes

- [ ] Break the builder/reporter utility into a separate project
- [ ] Prototype including current branch/version/MR information in the report
- [ ] Prototype a single Pages site with all the branches/versions included
- [ ] Parse a real JSON file
- [ ] Research dynamic-type tables
- [ ] Research SVG with transparent/differing backgrounds
- [ ] Try a dynamic SVG with Javascript
- [ ] Think about ways of reporting vulnerabilities that would be useful to developers


Break the builder/reporter utility into a separate project

- Ideally a single include with inputs
- Alongside other includes
- Report generator jobs
  - this report for this pipeline
  - mdbook-ready artifacts in reports/job-name/
- Report publisher job
  - Populates summary based on contents of reports
  - Might include other documentation
  - Runs mdbook
- Pages job

Step 1: Isolate the artifact flow

What are the actual stages?

1. Jobs that create output
2. report: Generate reports based on the output
3. consolidate: Consolidate the reports into one site for mdbook
4. pre-publish: MDBook - generate the site
5. publish: Pages
