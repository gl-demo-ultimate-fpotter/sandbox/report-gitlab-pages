from pathlib import Path

reportsdir = Path('reports')
with open(reportsdir / 'SUMMARY.md', 'w') as summaryfile, \
    open(reportsdir / 'index.md', 'w') as indexfile:
    indexfile.write('# Reports Index\n\n')
    summaryfile.write("[Index](index.md)\n")
    for reportdir in reportsdir.iterdir():
        if reportdir.is_dir():
            pagepath = reportdir / 'page.md'
            with open(pagepath) as pagefile:
                for line in pagefile:
                    if line.startswith('# '):
                        title = line[1:].strip()
                        link = pagepath.relative_to(reportsdir)
                        summaryfile.write(f"- [{title}]({link})\n")
                        indexfile.write(f"## [{title}]({link})\n\n")


    