from pathlib import Path
from pathlib import Path
import shutil

dir = Path('reports/coverage')
dir.mkdir(parents=True, exist_ok=True)

page = """
# Coverage

| Module         | Lines of Code | Lines Tested | Coverage (%) | Status      |
|----------------|---------------|--------------|--------------|-------------|
| Authentication | 1200          | 1100         | 91.7         | Excellent   |
| Database       | 1500          | 1400         | 93.3         | Excellent   |
| User Interface | 1800          | 1400         | 77.8         | Good        |
| API            | 1600          | 1300         | 81.3         | Good        |
| Utils          | 600           | 500          | 83.3         | Good        |
| Logging        | 400           | 300          | 75.0         | Needs Work  |
| Total          | 7100          | 6000         | 84.5         | Good        |
"""

with open(dir / 'page.md', 'w') as pagefile:
    pagefile.write(page)
