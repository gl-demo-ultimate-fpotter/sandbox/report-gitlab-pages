from pathlib import Path
import matplotlib.pyplot as plt

dir = Path('reports/sast')
dir.mkdir(parents=True, exist_ok=True)

categories = ['A', 'B', 'C', 'D', 'E']
values = [5, 7, 2, 4, 6]
plt.bar(categories, values)
plt.xlabel('Categories')
plt.ylabel('Values')
plt.title('SAST chart')
svgpath = dir / 'chart.svg'
plt.savefig(svgpath, format='svg')

page = """
# SAST

<img src="chart.svg"/>
"""

with open(dir / 'page.md', 'w') as pagefile:
    pagefile.write(page)
